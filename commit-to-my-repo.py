import argparse
import os
import git


def create_dict(directory):
    try:
        dict_path = os.path.join(f"/home/{repository}/", directory)
        os.mkdir(dict_path)
    except:
        return


def create_file(filename):
    new_file = open(f"/home/{repository}/{commit_branch}/{filename}", "w")
    new_file.write("Hello there you did it!")
    new_file.close()

    return new_file


def commit_file(file):
    repo.index.add(file)
    repo.index.commit("Commit test V1")


def push_file():
    origin = repo.remote(f"origin")
    origin.push()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # parser.add_argument("directory", help="Directory for file")
    parser.add_argument("filename", help="Filename to create and commit")
    args = parser.parse_args()
    filename = args.filename
    # directory = args.directory

    user = os.environ['USER']
    password = os.environ['PASS']
    commit_branch = os.environ['BRANCH']

    repopath = "AGrozdek/testproject"
    repository = "testproject"

    repo = git.Repo.clone_from(f"https://{user}:{password}@gitlab.com/{repopath}.git", repository)

    repo.git.fetch()
    repo.git.switch(commit_branch)

    create_dict(commit_branch)
    file = create_file(filename)
    commit_file(file.name)
    push_file()