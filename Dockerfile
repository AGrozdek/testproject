FROM python:latest

COPY requirements.txt .

RUN pip install -r requirements.txt

WORKDIR /home

COPY commit-to-my-repo.py .

ENTRYPOINT ["python"]
