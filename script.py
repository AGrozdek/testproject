from git import Repo
import os

commit_message = 'commit message'
branch_name = 'DBranch' 
repository = 'testproject'

repo = Repo(f"{os.getcwd()}")

try:
    branch = repo.create_head(branch_name, "HEAD")
    branch.checkout()
except:
    branch = [b for b in repo.branches if b.name == branch_name]
    branch = branch[0]
    branch.checkout()

file = open("script.txt", "w")
file.write("Hello there you did it!")

file = os.getcwd() + f'/{file.name}'

repo.index.add(file)

repo.index.commit(commit_message)
origin = repo.remote('origin')
origin.push(refspec=f'{branch}:{branch}')